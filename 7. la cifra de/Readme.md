# Description

I found this cipher in an old book. Can you figure out what it says? Connect with 
```
nc jupiter.challenges.picoctf.org 5726
```

# Solve
Воспользоваться Cryptool 1.4. 

Analysis -> Symmetric Encryption -> Ciphertext only -> Vigenere