# Description

I found the flag, but my brother wrote a program to encrypt all his text files. He has a spelling quiz study guide too, but I don't know if that helps.

# Solve
Для начала `unzip public.zip`.  
Посчитать частотность встечаемости букв (через криптотул, построив гистограму из тулзов для аналитики,
или использовав скрипт `solve.py`). Далее воспользоваться веб-переборщиком вариантов расшифровки ключа:
https://quipqiup.com/ . Читаемым оказжется только вариант про собаку
