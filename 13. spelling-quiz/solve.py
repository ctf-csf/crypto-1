if __name__ == '__main__':

    f = open('./public/study-guide.txt')
    files = f.readlines()
    files = list(map(lambda a: a.replace('\n', ''), files))

    letters = {}
    for word in files:
        for letter in word:
            if letter in letters.keys():
                letters[letter] += 1
            else:
                letters[letter] = 1

    print(letters)
