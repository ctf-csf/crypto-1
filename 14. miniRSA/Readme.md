# Description

What happens if you have a small exponent? There is a twist though, 
we padded the plaintext so that (M ** e) is just barely larger than 
N. Let's decrypt this: ciphertext

# Solve

Шифротекст образуется по формуле:
```math
plaintext^e mod N = ciphertext
```
Отсюда вывод: 
```math
plaintext^e = ciphertext + x * N
```
где x - некое число, которое нужно подобрать. Так как _e_ - 
очень маленькое число (3), то можно попробовать извлечь корень 
третьей степени и если X окажется целым числом, есть все основания полагать, 
что он и будет являться исходным текстом
```math
plaintext = \sqrt[3]{ciphertext + x * N}
```

