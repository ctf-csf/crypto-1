import pwn

KEY_LEN = 50000

if __name__ == '__main__':
    conn = pwn.connect('mercury.picoctf.net', 20266)
    conn.recvuntil(b'flag!\n')
    flag = conn.recvline(keepends=False).decode()
    pwn.log.info('FLAG ENCODE IS: {}'.format(flag))
    bin_flag = pwn.unhex(flag)

    col = KEY_LEN - len(bin_flag)
    conn.sendlineafter(b'What data would you like to encrypt? ', b'a' * col)

    conn.sendlineafter(b'What data would you like to encrypt? ', bin_flag)
    conn.recvuntil(b'ya go!\n')
    print(pwn.unhex(conn.recvline(keepends=False)).decode())
