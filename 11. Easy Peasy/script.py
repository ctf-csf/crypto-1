import pwn

KEY_LEN = 50000


if __name__ == '__main__':
    conn = pwn.connect('mercury.picoctf.net', 20266)
    conn.recvuntil(b'encrypted flag!\n')
    flag = conn.recvline(keepends=False).decode()
    bin_flag = pwn.unhex(flag)
    pwn.log.info(f'STRING ENCODE FLAG: {flag}')
    conn.recvuntil(b'to encrypt?')
    conn.sendline('a' * (KEY_LEN - len(bin_flag)))
    conn.sendlineafter('to encrypt?', bin_flag)
    conn.interactive()
