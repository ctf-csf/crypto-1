# Description

A one-time pad is unbreakable, but can you manage to recover the flag? (Wrap with picoCTF{}) nc mercury.picoctf.net 20266

# Solve

Воспользоваться библиотекой *pwntools*. Программа-шифровщик
ксорит приходящие данные с сторокой ключа, в которой 50000 символов. 
Флаг ксорится с первыми 32 символами. Когда символы ключа заканчиваются, 
ключ начинает применяться циклически, опять сначала.
Необходимо "промотать" все остальные символы, чтобы ключ "обнулился" и 
отправить туда ключ в бинарном представлении, чтобы он проксорился 
с теми же символами ключа.