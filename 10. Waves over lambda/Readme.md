# Description

We made a lot of substitutions to encrypt this. Can you decrypt it? Connect with  
```nc jupiter.challenges.picoctf.org 39894```

# Solve
Воспользоваться криптулом, вычистить текст от дефисов и нижних подчеркиваний, 
применить Substitution. Большие буквы - это те, которые заменились, маленькие - которые
не распознались. Необходимо руками подправить букы, ориентируясь по тому, что уже
распознано.  
Второй вариант - https://www.guballa.de/substitution-solver